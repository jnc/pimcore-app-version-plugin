pimcore.registerNS("pimcore.plugin.pimcoreappversion");

pimcore.plugin.pimcoreappversion = Class.create(pimcore.plugin.admin, {
    getClassName: function() {
        return "pimcore.plugin.pimcoreappversion";
    },

    initialize: function() {
        pimcore.plugin.broker.registerPlugin(this);
    },
 
    pimcoreReady: function (params,broker){
        // alert("PimcoreAppVersion Plugin Ready!");
    }
});

var pimcoreappversionPlugin = new pimcore.plugin.pimcoreappversion();

