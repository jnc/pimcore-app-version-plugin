# PimcoreAppVersion plugin

Pimcore plugin for managing application version. This plugin is designed for Pimcore version 4.6 and is to be installed via composer.


# Installation


## Composer
To install the plugin, first add the following repository to your `composer.json` file:

```javascript
"repositories": [
    {
      "type": "vcs", 
      "url": "https://bitbucket.org/jnc/pimcore-app-version-plugin.git"
    }
  ]

```

After adding the repository, install the plugin with: `composer require jnctech/pimcore-app-version`.

Run a `composer update` to ensure the plugin is installed and all auto-load files are genereated.

## Setup

As part of the plugin setup, the plugin creates a Website Setting: `PAV_GIT_ROOT`. This value must be set to the path of the git repo.

For example if the repo is cloned to `/vagrant/pimcore-app/.git` then the website setting value should be `/vagrant/pimcore-app`.
# Usage

The plugin is based around generating a `VERSION` file in the root of the Pimcore installation (`PIMCORE_DOCUMENT_ROOT`). 

If the Pimcore installation is running **DEV** mode, the `VERSION` file is ignored, and on every request the version number is read directly from git. If the site is not in dev mode, the `VERSION` file is read. 

Regardless of what method is used to determine the version, it is always set into an environment variable `PAV_VERSION`, also available as a constant at `JNCTech\PimcoreAppVersion\Plugin::PAV_VERSION_ENV_NAME`. 

The version can be read in PHP like so:

```php
echo 'Version: ' .  getenv(JNCTech\PimcoreAppVersion\Plugin::PAV_VERSION_ENV_NAME)
```

The `VERSION` file is maintained through a Pimcore command that reads the git repo, and outputs the tag, commit and branch into a version string and stored into the `VERSION` file.

The command should be ran on deployment to update the version to the latest. The command can be executed using the Pimcore console framework:

```sh
php /var/www/html/pimcore/cli/console.php app:version:write
```