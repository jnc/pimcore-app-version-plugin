<?php

namespace JNCTech\PimcoreAppVersion;

use Pimcore\API\Plugin as PluginLib;
use JNCTech\PimcoreAppVersion\Service\VersionManager;

class Plugin extends PluginLib\AbstractPlugin implements PluginLib\PluginInterface
{
    const PAV_GIT_ROOT_SETTING_NAME = 'PAV_GIT_ROOT';
    const PAV_VERSION_FILE_PATH = PIMCORE_DOCUMENT_ROOT . '/' . 'VERSION';
    const PAV_VERSION_ENV_NAME = "PAV_VERSION";

    public function init()
    {
        parent::init();

        \Pimcore::getEventManager()->attach('system.console.init', function(\Zend_EventManager_Event $e) {
            /** @var \Pimcore\Console\Application $application */
            $application = $e->getTarget();

            // add a namespace to autoload commands from
            $application->addAutoloadNamespace('JNCTech\\PimcoreAppVersion\\Command', PIMCORE_DOCUMENT_ROOT . '/plugins/PimcoreAppVersion/lib/Command');

            Plugin::setVersionVariable();
        });

        \Pimcore::getEventManager()->attach('system.startup', function(\Zend_EventManager_Event $e) {
            Plugin::setVersionVariable();
        });
    }

    public function handleDocument($event)
    {
        // do something
        $document = $event->getTarget();
    }

    public static function install()
    {
        //create website setting
        if(!\Pimcore\Model\WebsiteSetting::getByName(Plugin::PAV_GIT_ROOT_SETTING_NAME)){
            $gitWebsiteSetting = new \Pimcore\Model\WebsiteSetting();
            $gitWebsiteSetting->setName(Plugin::PAV_GIT_ROOT_SETTING_NAME);
            $gitWebsiteSetting->setType('text');
            $gitWebsiteSetting->setData('/var/www/html');
            $gitWebsiteSetting->save();
        }

        //create version file
        $versionFile = fopen(Plugin::PAV_VERSION_FILE_PATH, 'w');
        fwrite($versionFile, "0.0");
        fclose($versionFile);

        return "Installed!";
    }

    public static function uninstall()
    {        
        $setting = \Pimcore\Model\WebsiteSetting::getByName(Plugin::PAV_GIT_ROOT_SETTING_NAME);

        if(!!$setting){
            $setting->delete();
        }

        return "Uninstalled!";
    }

    public static function isInstalled()
    {
        $setting = !!\Pimcore\Model\WebsiteSetting::getByName(Plugin::PAV_GIT_ROOT_SETTING_NAME);
        return $setting;
    }

    private static function setVersionVariable(){
        //set version environment variable
        if(PIMCORE_DEVMODE === true){
            try{
                $vm = new VersionManager();
                putenv(self::PAV_VERSION_ENV_NAME . '=' . $vm->getVersion());
            }
            catch(\Exception $e){
                putenv(self::PAV_VERSION_ENV_NAME . '=' . 'VERSION PLUGIN NEEDS CONFIG');
            }
            
        }
        else{
            putenv(self::PAV_VERSION_ENV_NAME . '=' . file_get_contents(self::PAV_VERSION_FILE_PATH));
        }
    }
}
