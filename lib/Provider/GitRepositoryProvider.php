<?php

namespace JNCTech\PimcoreAppVersion\Provider;

use RuntimeException;

/**
 * Class GitRepositoryProvider
 */
class GitRepositoryProvider implements ProviderInterface
{
    /**
     * @var string
     */
    private $path;

    /**
     * Constructor
     *
     * @param $path
     */
    public function __construct($path)
    {
        $this->path = $path;
    }

    /**
     * @return bool
     */
    public function isSupported()
    {
        return $this->isGitRepository($this->path) && $this->canGitDescribe();
    }

    /**
     * @return string
     * @throws RuntimeException
     */
    public function getVersion()
    {
        return $this->getGitDescribe();
    }

    /**
     * @param   string $path
     * @return  boolean
     */
    private function isGitRepository($path)
    {
        if (is_dir($path . DIRECTORY_SEPARATOR . '.git')) {
            return true;
        }
        else if(file_exists($this->path)){
            chdir($this->path);
            $isBareRepo = exec('git rev-parse --is-bare-repository 2>&1', $output, $returnCode);
            if($isBareRepo === "true"){
                return true;
            }
        }

        $path = dirname($path);

        if (strlen($path) == strlen(dirname($path))) {
            return false;
        }

        return $this->isGitRepository($path);
    }

    /**
     * If describing throws error return false, otherwise true
     *
     * @return boolean
     */
    private function canGitDescribe()
    {
        try {
            $this->getGitDescribe();
        } catch (RuntimeException $e) {
            return false;
        }

        return true;
    }

    /**
     * @return string
     * @throws RuntimeException
     */
    private function getGitDescribe()
    {
        $dir = getcwd();
        chdir($this->path);
        $tag = exec('git describe --tags 2>&1', $output, $returnCode);
        chdir($dir);

        if ($returnCode !== 0) {
            throw new RuntimeException('Git error: ' . $tag);
        }

        $branch = exec('git branch | grep \* | cut -d \' \' -f2', $output, $returnCode);
        
        if ($returnCode !== 0) {
            throw new RuntimeException('Git error: ' . $branch);
        }

        if($branch == 'master'){
            return $tag;
        }

        return $tag . '-' . $branch;
    }
}
