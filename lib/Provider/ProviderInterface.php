<?php

namespace JNCTech\PimcoreAppVersion\Provider;

/**
 * Interface for version provider
 */
interface ProviderInterface
{
    /**
     * @return boolean
     */
    public function isSupported();

    /**
     * @return string
     */
    public function getVersion();
}