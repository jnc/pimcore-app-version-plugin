<?php

namespace JNCTech\PimcoreAppVersion\Service;

use RuntimeException;
use JNCTech\PimcoreAppVersion\Provider\ProviderInterface;
use JNCTech\PimcoreAppVersion\Provider;
use JNCTech\PimcoreAppVersion\Plugin;

/**
 * Class VersionManager
 */
class VersionManager
{
    /**
     * @var array
     */
    private $providers;

    /**
     * @var array
     */
    private $activeProvider;

    public function __construct()
    {
        $this->providers = array();
        $this->activeProvider = null;

        $this->addProviders();
    }

    private function addProviders(){
        $gitRoot = \Pimcore\Model\WebsiteSetting::getByName(Plugin::PAV_GIT_ROOT_SETTING_NAME)->getData();
        $this->addProvider(new Provider\GitRepositoryProvider($gitRoot), "git", 0);
    }

    /**
     * @param ProviderInterface $provider
     * @param string            $alias
     * @param integer           $priority
     */
    private function addProvider(ProviderInterface $provider, $alias, $priority)
    {
        $this->providers[$alias] = array(
            'provider' => $provider,
            'priority' => $priority,
            'alias' => $alias
        );

        // sort providers by priority
        uasort(
            $this->providers,
            function ($a, $b) {
                if ($a['priority'] == $b['priority']) {
                    return 0;
                }

                return $a['priority'] < $b['priority'] ? 1 : -1;
            }
        );
    }

    /**
     * @return Version
     */
    public function getVersion()
    {
        $provider = $this->getSupportedProvider();

        return $provider->getVersion();
    }

    /**
     * @return ProviderInterface
     */
    public function getActiveProvider()
    {
        return $this->activeProvider['provider'];
    }

    /**
     * Returns array of registered providers
     *
     * @return array
     */
    public function getProviders()
    {
        return $this->providers;
    }

    /**
     * @return ProviderInterface
     * @throws RuntimeException
     */
    public function getSupportedProvider()
    {
        if (empty($this->providers)) {
            throw new RuntimeException('No versioning provider found');
        }

        foreach ($this->providers as $entry) {
            $provider = $entry['provider'];
            /** @var $provider ProviderInterface */
            if ($provider->isSupported()) {
                $this->activeProvider = $entry;

                return $provider;
            }
        }

        throw new RuntimeException('No supported versioning providers found');
    }
}
