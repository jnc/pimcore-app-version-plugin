<?php

namespace JNCTech\PimcoreAppVersion\Command;

use \Pimcore\Cache;
use \Pimcore\Console\AbstractCommand;
use \Pimcore\Model\Object\ClassDefinition;
use \Pimcore\Model\Object;
use \Pimcore\Model\Asset;
use \Symfony\Component\Console\Input\InputInterface;
use \Symfony\Component\Console\Input\InputOption;
use \Symfony\Component\Console\Output\OutputInterface;
use \Symfony\Component\Console\Input\InputArgument;
use \Symfony\Component\Console\Helper\Table;
use JNCTech\PimcoreAppVersion\Service\VersionManager;

class ListProvidersCommand extends AbstractCommand
{
    /**
     * @var VersionManager
     */
    private $manager;

    /**
     * Constructor
     *
     * @param VersionManager $manager
     */
    public function __construct()
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('app:version:list-providers')
            ->setDescription(
                'List all available version providers'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->manager = new VersionManager(); 
       
        $this->listProviders($output);
    }

    /**
     * @param OutputInterface $output
     */
    protected function listProviders(OutputInterface $output)
    {
        $output->writeln('Registered version providers');
        $providers = $this->manager->getProviders();
        $table = new Table($output);
        $table->setHeaders(array('Alias', 'Class', 'Priority', 'Supported'))
            ->setStyle('borderless');

        foreach ($providers as $alias => $providerEntry) {
            /** @var $provider ProviderInterface */
            $provider = $providerEntry['provider'];
            $supported = $provider->isSupported() ? 'Yes' : 'No';
            $table->addRow(array($alias, get_class($provider), $providerEntry['priority'], $supported));
        }

        $table->render();
    }
}
