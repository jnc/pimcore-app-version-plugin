<?php

namespace JNCTech\PimcoreAppVersion\Command;

use Pimcore\Cache;
use Pimcore\Console\AbstractCommand;
use Pimcore\Model\Object\ClassDefinition;
use Pimcore\Model\Object;
use Pimcore\Model\Asset;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Helper\Table;
use JNCTech\PimcoreAppVersion\Service\VersionManager;
use JNCTech\PimcoreAppVersion\Plugin;

class VersionWriteCommand extends AbstractCommand
{
    protected function configure()
    {
        $this
            ->setName('app:version:write')
            ->setDescription(
                'Bump application version using one of the available providers'
            )
            ->addOption('dry-run', 'd', InputOption::VALUE_NONE, 'Dry run, does not update VERSION file');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /**
        * @var VersionManager
        */
        $manager = new VersionManager();
        $version = $manager->getVersion();

        $output->writeln(sprintf('Provider: <comment>%s</comment>', get_class($manager->getActiveProvider())));
        $output->writeln(sprintf('Current version: <info>%s</info>', getenv(Plugin::PAV_VERSION_ENV_NAME)));
        $output->writeln(sprintf('New version: <info>%s</info>', $version));
        if ($input->getOption('dry-run')) {
            $output->writeln(sprintf('<question>%s</question>', 'Dry run, skipping version bump'));
        } else {
            $this->writeVersionFile($version);
        }
    }

    /**
     * @param string $version
     */
    protected function writeVersionFile($version)
    {
        $versionFile = fopen(Plugin::PAV_VERSION_FILE_PATH, 'w');
        fwrite($versionFile, $version);
        fclose($versionFile);
    }
}